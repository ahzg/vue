import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import LifeCycle from '@/components/LifeCycle'
import Instructions from '@/components/Instructions'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/life',
      name: 'LifeCycle',
      component: LifeCycle
    },
    {
      path: '/instructions',
      name: 'Instructions',
      component: Instructions
    }
  ]
})
